const jwt = require('jsonwebtoken');
const refreshtoken = require('rand-token');
const logger = require('./../log-config/config');
const config = require('../env-config').config;
const ErrorController = require('./../controllers/error-controller');
const repository = require('./../repository');
const ErrorEnums = require('./../utils/errorEnum');


module.exports = function JwtController(){
  const errorController = new ErrorController();

  this.createJwt = async (data) => {
    logger.info({
      module: 'jwt-controller',
      method: 'creating JWT',
      description: `getting creating jwt`
    });
    const refresh = refreshtoken.uid(8);

    const payload = {
      ...data.payload,
      refresh_token: refresh
    };

    const token = jwt.sign(payload, config.jwtKey, data.options || {
      expiresIn: config.expiresIn
    });
    logger.info({
      module: 'jwt-controller',
      method: 'create jwt',
      description: `refresh_token: ${JSON.stringify(refresh)}`
    });

    await repository.setKey(refresh, token);

    return { access_token: token, refresh_token: refresh, expiresIn:config.expiresIn };
  };

  this.refreshJwt = async (data, jwtRefresh) => {
    logger.info({
      module: 'jwt-controller',
      method: 'refresh JWT',
      description: `refreshing jwt`
    });
    const payload = {
      ...data.payload,
      refresh_token: jwtRefresh
    };

    const cachedToken = await repository.getKey(jwtRefresh);

    if(cachedToken){
      const token = jwt.sign(payload, config.jwtKey, data.options || {
        expiresIn: config.expiresIn
      });
      logger.info({
        module: 'jwt-controller',
        method: 'refreshToken',
        description: `refreshing token`
      });
      const refresh = refreshtoken.uid(8);

      //Erase the old token
      await repository.removeKey(jwtRefresh);
      //Save the new token with new key
      await repository.setKey(refresh, token);

      return { access_token: token, refresh_token: refresh, expiresIn:config.expiresIn };
    }

    throw errorController.throw(ErrorEnums.INVALID_REFRESH_TOKEN);
  };

  this.verifyJwt = async (token) => {
    logger.info({
      module: 'jwt-controller',
      method: 'creating JWT',
      description: `getting verify jwt`
    });

    const response = jwt.verify(token, config.jwtKey, {}, (err, decoded) => {
      if (err) {
        throw errorController.throw(ErrorEnums.INVALID_TOKEN);
      } else if (!err) {
        decoded.valid = true;

        return decoded;
      }
    });

    const cachedToken = await repository.getKey(response.refresh_token);

    if (!cachedToken) {
      throw errorController.throw(ErrorEnums.INVALID_TOKEN);
    }

    logger.info({
      module: 'jwt-controller',
      method: 'verifyJwt',
      description: `response: ${JSON.stringify(response)}`
    });

    return response;
  };

  this.removeJwt = async (jwtRefresh) => {
    logger.info({
      module: 'jwt-controller',
      method: 'creating JWT',
      description: `getting remove jwt`
    });

    await repository.removeKey(jwtRefresh);

    logger.info({
      module: 'jwt-controller',
      method: 'remove-jwt',
      description: `removed credentials`
    });

    return {
      removed: true
    };
  };
};

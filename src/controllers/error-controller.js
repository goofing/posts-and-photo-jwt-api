const errorEnums = require('./../utils/errorEnum');

module.exports = function ErrorController() {
  this.throw = (err) => {
    return { ...err, isLocal: true };
  };

  const handleControlledErrors = (err) => {
    return errorEnums[err.code];
  };

  const handleUncontrolledErrors = (err) => {
    let objectError = '';
    if (err.code) {
      objectError = errorEnums[err.code];
    }

    return objectError?objectError:errorEnums.SOMETHING_WENT_WRONG;
  };

  const buildResponse = (response) => {
    const errResp = {
      err: {},
      statusCode: 500
    };
    if (response.statusCode) {
      errResp.statusCode = response.statusCode ;
    }
    errResp.err = { ...response };
    delete errResp.err.statusCode;

    return errResp;
  };

  this.handle = (err) => {
    let response = {};
    if (err instanceof Object && err.isLocal) {
      response = handleControlledErrors(err);
    } else {
      response = handleUncontrolledErrors(err);
    }

    return buildResponse(response);
  };
};

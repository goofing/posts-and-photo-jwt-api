const enums = {
  REFRESH_TOKEN_NOT_FOUND: { code:'REFRESH_TOKEN_NOT_FOUND', message:'Refresh token not found', statusCode: 400 },
  INVALID_REFRESH_TOKEN: { code:'INVALID_REFRESH_TOKEN', message:'Refresh token invalid', statusCode: 400 },
  INVALID_TOKEN: { code:'INVALID_TOKEN', message:'invalid token', statusCode: 400 },
  MALFORMED_REQUEST: { code:'MALFORMED_REQUEST', message:'Malformed body', statusCode: 400 },
  SOMETHING_WENT_WRONG: { code:'SOMETHING_WENT_WRONG', message:'Something went wrong' },
  WRONG_CREDENTIALS: { code:'WRONG_CREDENTIALS', message:'Wrong credentials' },
};

module.exports = enums;
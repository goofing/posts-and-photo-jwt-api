const HttpRequest = require('./httpRequest');

module.exports = function() {
  const httpRequest = new HttpRequest();

  const getHeaders = (token) => {
    const obj = {};
    if (token) {
      obj['Authorization'] = `Bearer ${token}`;
    }

    return obj;
  };

  this.get = (url, token = '') => {
    const headers = getHeaders(token);

    return httpRequest.prepare({
      method: 'get',
      url: url,
      timeout: 3000,
      headers
    });
  };
};
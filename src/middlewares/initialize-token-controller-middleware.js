const JwtController = require('./../controllers/jwt-controller');

module.exports = async (req, res, next) => {
  req.jwtController = new JwtController();

  return next();
};
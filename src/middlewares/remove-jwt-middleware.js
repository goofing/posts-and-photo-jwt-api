const logger = require('./../log-config/config');

module.exports = async (req, res, next) => {
  try {
    req.result = await req.jwtController.removeJwt(req.body.refresh_token);

    return next();
  } catch (error) {
    logger.info({ module: 'authenticate-middleware', method: 'authenticateMiddleware', description: `Error: ${error.message}` });

    return next(error);

  }
};

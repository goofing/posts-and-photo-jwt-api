const config = require('../env-config').config;

const redis = require('redis');


let client = null;
const getClient = async () => {
  if (!client) {
    client = await redis.createClient({
      host: config.redisHost, port: 6379
    });
    client.on('error', function(error) {
      console.error(error);
    });
  }
  
  return client;
};

const setKey = async (key, value) => {
  const cli = await getClient();
  cli.set(key, value);
  client.expire(key, (60 * 5));

};

const getKey = async (key) => {
  const cli = await getClient();

  const p = new Promise((resolve) =>{
    cli.get(key, (err,reply) =>{
      console.log(reply);
      resolve(reply);
    });
  });

  const res = await p.then(t => t);

  return res;
};

const removeKey = async (key) => {
  const cli = await getClient();
  cli.del(key, redis.print);
};


module.exports = {
  setKey,
  getKey,
  removeKey
};
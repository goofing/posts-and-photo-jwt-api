module.exports = {
  jwtKey: process.env.JWT || 'FYGi886rLEYCiGw0D888CzVT9CkrIkjZUSR',
  expiresIn: process.env.EXPIRE_IN_JWT || '5m',
  redisHost: process.env.REDIS_HOST || '127.0.0.1'
};

module.exports = {
  create: [
    'initialize-token-controller-middleware',
    'generate-jwt-middleware',
    'common-response'
  ],
  remove:[
    'initialize-token-controller-middleware',
    'remove-jwt-middleware',
    'common-response'
  ],
  verify:[
    'initialize-token-controller-middleware',
    'verify-jwt-middleware',
    'common-response'
  ],
  refresh:[
    'initialize-token-controller-middleware',
    'refresh-jwt-middleware',
    'common-response'
  ]
};

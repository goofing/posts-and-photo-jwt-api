const { readdirSync } = require('fs');
const baseDir = `${__dirname}/../../middlewares`;
const definitions = require('./specifications');

const getDirectories = (source) => readdirSync(source, { withFileTypes: true });

const addMiddlewares = (middlewareDir, baseDir, name, arrayOfMiddleware) => {
  const exist = middlewareDir.find(
    (object) => object.name.split('.')[0] === name
  );
  if (!exist) {
    const subDirs = middlewareDir.filter((object) => object.isDirectory());
    if (subDirs.length === 0) {
      throw `The middleware ${name} doesn't exist, please check if its name is the right one`;
    }
    subDirs.forEach((dir) => {
      const newDirectory = getDirectories(`${baseDir}/${dir.name}`);
      addMiddlewares(
        newDirectory,
        `${baseDir}/${dir.name}`,
        name,
        arrayOfMiddleware
      );
    });
  } else {
    const middleware = require(`${baseDir}/${name}`); // eslint-disable-line
    arrayOfMiddleware.push(middleware);
  }
};

const getMiddlewares = (middlewareNames) => {
  const middlewareDir = getDirectories(baseDir);
  const arrayOfMiddleware = [];

  middlewareNames.forEach((name) => {
    addMiddlewares(middlewareDir, baseDir, name, arrayOfMiddleware);
  });

  return arrayOfMiddleware;
};

module.exports.getMiddlewares = getMiddlewares;
module.exports.definitions = definitions;

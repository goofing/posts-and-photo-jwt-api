const express = require('express');
const middlewareSpecifications = require('./middlewares-specifications')
  .definitions;
const { getMiddlewares } = require('./middlewares-specifications');
const router = express.Router();

router.post('/jwt', getMiddlewares(middlewareSpecifications.create));
router.post('/verify', getMiddlewares(middlewareSpecifications.verify));
router.post('/remove', getMiddlewares(middlewareSpecifications.remove));
router.post('/refresh', getMiddlewares(middlewareSpecifications.refresh));

module.exports = router;
// setting the env-config
const info = ({ module, method, description }) => {
  console.log(`time:${new Date()},file: ${module}, method:${method}, description:${description}`);
};
// setting the env-config
const error = ({ module, method, description }) => {
  console.error(`time:${new Date()},file: ${module}, method:${method}, description:${description}`);
};
module.exports.info = info;
module.exports.error = error;

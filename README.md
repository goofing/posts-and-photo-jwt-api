# JWT-API

## Installation

1. Install node v12
   `nvm instal 12`

2. Clone this repository 😊
   `git clone ....`

3. Install dependencies
   `npm install`

4. Run project on develop mode
   `NODE_ENV=local npm run start`

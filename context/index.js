const context = {
  name: process.env.NODE_CONTEXT_NAME || 'jwt-api',
  port: process.env.SERVER_PORT || '8081',
  version: process.env.NODE_CONTEXT_VERSION || 'v1',
  baseURI: process.env.NODE_CONTEXT_BASE_URI || 'jwt-test',
};
module.exports = context;

FROM node:12.18.1-alpine3.9
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY . .

ENV NODE_ENV local
EXPOSE 8080

CMD [ "npm" ,"run", "start" ]
